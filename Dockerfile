FROM tiangolo/uwsgi-nginx-flask:flask
ENV UWSGI_INI /app/uwsgi.ini
COPY ./app /app

# Copy over our requirements.txt file
COPY requirements.txt /tmp/

# Upgrade pip and install required python packages
RUN pip install -U pip
RUN pip install -r /tmp/requirements.txt
