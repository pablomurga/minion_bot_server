import telegram
from telegram.ext import Updater #pip install python-telegram-bot
from telegram.ext import CommandHandler


class TelegramBot():

    def __init__(self):
        self.bot = telegram.Bot(token='') #see README.md for more info
        self.group_id = #Should be a 13 digit integer (see README.md for more info)
        self.updater = Updater(self.bot.token)
        self.dispatcher = self.updater.dispatcher
        self.send_ranking_handler = CommandHandler('send_ranking', self.send_ranking)
        self.dispatcher.add_handler(self.send_ranking_handler)
        self.updater.start_polling()

    def send_ranking(self, ranking):
        self.bot.sendMessage(chat_id=self.group_id, text="__________________________________")
        self.bot.sendMessage(chat_id=self.group_id, text="Los tickets de la verguenza son: \n")
        for ticket in ranking:
            self.bot.sendMessage(chat_id=self.group_id, text="- Title: {} \n- Owner: {} \n- Days: {} \n- Source: {}\n".format(ticket.get('title').encode('utf-8'), ticket.get('name').encode('utf-8'), ticket.get('days'), ticket.get('source').encode('utf-8')))
        self.bot.sendMessage(chat_id=self.group_id, text="__________________________________")
