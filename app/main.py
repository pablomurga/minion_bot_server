#!flask/bin/python
from flask import Flask, request, g
from flask_expects_json import expects_json
import json
import os
import threading
import logging
from operator import itemgetter
from telegram_bot import TelegramBot

app = Flask(__name__)
logfile = "/tmp/minion_bot_server.log"

telegram_bot = TelegramBot()

#Setup logging
logging.basicConfig(filename=logfile, level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logging.info('Started')

#For JSON validation
shame_schema = {
    "type": "object",
    "properties": {
        "tickets": {
            "type" : "array",
            "items": {
                "type": "object",
                "required": ["days", "name", "source", "title"],
                "properties": {
                    "days": {"type": "integer"},
                    "name": {"type": "string"},
                    "source": {"type": "string"},
                    "title": {"type": "string"}
                }
            }
        }
    }
}

#Helper class for background tasks
class AsyncRanking(threading.Thread):

    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        self.tickets = kwargs.get('tickets')

    def run(self):
        with app.app_context():
           self.ranking(self.tickets)

    def ranking(self, tickets):
        newlist = sorted(tickets, key=itemgetter('days'), reverse=True)
        telegram_bot.send_ranking(newlist)

@app.route('/shame', methods=['POST'])
@expects_json(shame_schema)
def rank():
    async_rank = AsyncRanking(**g.data)
    async_rank.start()
    logging.info('[POST] /shame with valid json')
    return json.dumps({'STATUS':'OK - Triggered async ranking'})

@app.route('/health', methods=['GET'])
def health():
    logging.info('[GET] /health - Triggered health check')
    return json.dumps({'STATUS':'OK'})


if __name__ == "__main__":

    app.run(host='0.0.0.0', debug=True, port=80, threaded=True)
