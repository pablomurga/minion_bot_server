# Pasos para deployar desde cero el entorno (en Ubuntu)

~~~
mkdir minion-bot-server

cd minion-bot-server

apt-get update -y

apt-get install python-dev python-pip git docker.io -y

sudo pip install virtualenv

git clone https://pablomurga@bitbucket.org/pablomurga/minion_bot_server.git /tmp/repo

cp -R /tmp/repo/* .

virtualenv venv

source venv/bin/activate

sudo pip install -r requirements.txt
~~~

La estructura de directorios debería quedar así (es importante):

~~~
|-minion-bot-server
|-app
  |-main.py
  |-uwsgi.ini
|-Dockerfile
|-requirements.txt
|-README.md
~~~

# Para buildear y deployar

Docker Build (hay que hacerlo cada vez que se modifica el código - estando parados en la carpeta minion-bot-server):

`sudo docker build -t minion_bot_server .`

Docker Run:

`docker run --name=minion_bot_server -d --restart=always -p 80:80 -t minion_bot_server`

## Logging

Logs del container se envían a stdout. Se pueden ver haciendo:

`docker logs --follow minion_bot_server`

Para los logs de la API hay que acceder al container directamente dentro de la instancia que corre la misma:

~~~~
- Ver los containers activos:
docker ps

- Debería mostrarnos uno llamado minion_bot_server, al cual podemos conectarnos:
docker exec -ti minion_bot_server bash

- Dentro del container hacer tail al log:
tail -f /tmp/minion_bot_server.log
~~~~
